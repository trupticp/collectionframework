package com.npci.collectionsInterfaces;

import java.util.ArrayList;
import java.util.List;

public class ListInterface {

	public static void main(String[] args) {
		List<String> list =new ArrayList<>();
		
//	list.add("element1");
//	list.add("element2");
//	list.add("element1");
//	list.add("element1");
//	
//    System.out.println(list);
//  list.add(null);
//  list.add(null);
//  System.out.println(list);

		
		//iterative order
		list.add("element1");//index 0
		list.add("element2");//index  1
		list.add("element3");//indx 2
		list.add("element4");//index 3
		
		System.out.println(list);
		
		System.out.println(list.get(0));
		System.out.println(list.get(3));
	}

}
