package com.npci.collectionsInterfaces;

import java.util.ArrayList;
import java.util.Collection;

public class Demo {

	public static void main(String[] args) {
		
           Collection<String> fruitcollection =new ArrayList<>();
           
           fruitcollection.add("Mango");
           fruitcollection.add("apple");
           fruitcollection.add("pear");
           System.out.println(fruitcollection);
           
           fruitcollection.remove("Mango");
           System.out.println(fruitcollection);
           
           
           System.out.println(fruitcollection.contains("cake"));
           fruitcollection.forEach((fruit)->{
        	   System.out.println(fruit);
           });
           
           fruitcollection.clear();
           System.out.println(fruitcollection);
           
	}

}

