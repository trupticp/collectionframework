package com.npci.collectionsarraylist;

import java.util.LinkedList;

public class CreateLinkedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  LinkedList<String> fruits = new LinkedList<>();
  
  fruits.add("Banana");
  fruits.add("Apple");
  fruits.add("Kiwi");
  fruits.add("Orange");
  System.out.println(fruits);
  
  //add an element at specified position
  
  fruits.add(2,"Lemon");
  fruits.addFirst("Grapes");
  fruits.add(3,"Cherry");
  
  System.out.println(fruits);
	}

}
