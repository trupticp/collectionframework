package com.npci.collectionsarraylist;
import java.util.*;  

public class HashSetRemove {

	public static void main(String[] args) {
	
	  HashSet<String> set=new HashSet<String>();  
		           set.add("Trupti");  
		           set.add("Anagha");  
		           set.add("Teena");  
		           set.add("Shital");  
		           System.out.println("An initial list of elements: "+set);  
		           
		           
		           
		           //addition of null
		           set.add("Null");  
		           set.add("Null");  
		           //Removing specific element from HashSet 
		           set.remove("Trupti");  
		           System.out.println("After invoking remove(object) method: "+set);  
		           
	  HashSet<String> set1=new HashSet<String>();  
		           
		           set1.add("Pooja");  
		           set1.add("Deepa");  
		           
		           set.addAll(set1);  
		           System.out.println("Updated List: "+set);  
		           //Removing all the new elements from HashSet  
			          set.removeAll(set1);  
			           System.out.println("After invoking removeAll() method: "+set);  
			           
			           //Removing elements on the basis of specified condition  
			           set.removeIf(str->str.contains("Anagha"));    
			           System.out.println("After invoking removeIf() method: "+set);  
			           //Removing all the new elements from HashSet  
				          // set.removeAll(set1);  
				           //System.out.println("After invoking removeAll() method: "+set);  
				           
				           //Removing elements on the basis of specified condition  
				           set.removeIf(str->str.contains("Anagha"));    
				           System.out.println("After invoking removeIf() method: "+set);  
		           
		           //Removing all the elements available in the set  
		           set.clear();  
		           System.out.println("After invoking clear() method: "+set);  
		 }  
		}  
	


