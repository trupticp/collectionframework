package com.npci.collectionsarraylist;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IterateOverList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  List<String> courses =Arrays.asList("c","c++","python");
  //basic loop
//  int len=courses.size();
//  for(int i=0; i<len;i++) {
//	  System.out.println(courses.get(i));
//	  
//  }
  
  
  //enhanced for each loop
  for(String course:courses) {
	  System.out.println(course);
  }
  
  //basic loop with iterator
  for(Iterator<String> iterator =courses.iterator();iterator.hasNext();) {
	  String course=(String) iterator.next();
	  System.out.println(course);
	  
  }
  
  
	}

}
