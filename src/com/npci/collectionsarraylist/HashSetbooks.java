package com.npci.collectionsarraylist;
import java.util.*;  
class Book {  
int id;  
String name,author,publisher;  
int quantity;  

public Book(int id, String name, String author, String publisher, int quantity) {  
    this.id = id;  
    this.name = name;  
    this.author = author;  
    this.publisher = publisher;  
    this.quantity = quantity;  
}}

class HashSetbooks {
	public  static void main(String[] args) {
	HashSet<Book> set=new HashSet<Book>();  
	
    //Creating Books  
    Book b1=new Book(1002,"Wings of fire","Arun Tiwari","Generic",20);  
    Book b2=new Book(1012,"Harry potter","J.k rowling","Bloomsburry books",50);  
    Book b3=new Book(1023,"The  theory of everything","Stephen Hawking","Jaico",6);  
    
    //Adding Books to HashSet  
    set.add(b1);  
    set.add(b2);  
    set.add(b3);  
    //Traversing HashSet  
    for(Book b:set){  
    System.out.println(b.id+" "+b.name+" "+b.author+" "+b.publisher+" "+b.quantity);  
    }  
}  
		
		
}
