package com.npci.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// List<Integer> list =new ArrayList<Integer>();
//   
// list.add(10);
// list.add(20);
// list.add(30);
// list.add(40);
// list.add(50);
// list.add(60);
// 
// System.out.println(list);
//Collections.sort(list);// ascending order
// //System.out.println(list);
//  Collections.reverse(list);
//  System.out.println(list);

		List<Employee> employees = new ArrayList<Employee>();

		employees.add(new Employee(0, "Venkat", 32, 4000000));
		employees.add(new Employee(1, "Trupti", 23, 2000000));
		employees.add(new Employee(2, "Teena", 22, 4500000));
		employees.add(new Employee(3, "Anagha", 23, 9000000));
		employees.add(new Employee(4, "Sheetal", 23, 5000000));

		//Collections.sort(employees, new MySort());

//		System.out.println(employees);
//		Collections.reverse(employees);
//		System.out.println(employees);

	



Collections.sort(employees, (o1,o2) -> (o1.getName().compareTo(o2.getName())));
    System.out.println(employees);
    
class MySort implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub

		// true=1, and false=-1
		return (int) (o1.getSalary() - o2.getSalary());

	}
	}

	}
	}
