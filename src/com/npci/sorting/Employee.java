package com.npci.sorting;

public class Employee {

	private int d;
	private String name;
	private int age;
	private long salary;
	
	public int getD() {
		return d;
	}
	public void setD(int d) {
		this.d = d;
	}
	public String getName() {
		return name;
	}
	public Employee(int d, String name, int age, long salary) {
		super();
		this.d = d;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public long getSalary() {
		return salary;
	}
	public void setSalary(long salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [d=" + d + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}
 
	}


